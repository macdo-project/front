export default class ApiManager {

    static base_url = "http://localhost:8000"

    static auth(id){
        return new Promise(((resolve, reject) => {
            fetch(ApiManager.base_url + '/auth', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({idBorne: id})
            }).then((res) => {
                res.json().then((json) => {
                    if (json.status === 200) {
                        resolve(json.data)
                    } else {
                        reject(json.errors)
                    }

                })
            })
        }))
    }

    static login(email, password) {

        return new Promise((resolve, reject) => {

            fetch(ApiManager.base_url + '/login', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({ email: email, password: password })
            }).then((res) => {
                res.json().then((json) => {
                    if (json.status === 200) {
                        resolve(json.data)
                    } else {
                        reject(json.errors)
                    }

                })

            })

        })

    }

    static getProducts() {
        return new Promise((resolve, reject) => {

            fetch(ApiManager.base_url + '/product', {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json'
                },
            }).then((res) => {
                res.json().then((json) => {
                    if (json.status === 200) {
                        resolve(json.data)
                    } else {
                        reject(json.errors)
                    }

                })

            })

        })
    }

    static getProductsFromCategory(id) {
        return new Promise((resolve, reject) => {

            fetch(ApiManager.base_url + '/product/category/' + id, {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json'
                },
            }).then((res) => {
                res.json().then((json) => {
                    if (json.status === 200) {
                        resolve(json.data)
                    } else {
                        reject(json.errors)
                    }

                })

            })

        })
    }

    static getCategories() {
        return new Promise((resolve, reject) => {

            fetch(ApiManager.base_url + '/categories', {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json'
                },
            }).then((res) => {
                res.json().then((json) => {
                    if (json.status === 200) {
                        resolve(json.data)
                    } else {
                        reject(json.errors)
                    }

                })

            })

        })
    }
}
