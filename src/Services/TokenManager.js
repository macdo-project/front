import decode from 'jwt-decode';

export default class TokenManager {

    static clear(){
        localStorage.clear();
    }

    static isSet(){
        // eslint-disable-next-line
        return localStorage.getItem('token') != undefined;
    }

    static isKitchen(){
        return this._decode().kitchen;
    }

    static isAdmin(){
        return this.isSet() && this._decode().type === 'User';
    }

    static set(token){
        localStorage.setItem('token', token);
    }

    static getId(){
        return this._decode().id;
    }

    static _decode(){
        return decode(localStorage.getItem('token'))
    }
}
