import {Link} from "react-router-dom";

export default function Header(){
    return (
        <header className="navbar navbar-expand-lg navbar-absolute">
            <div className="container-fluid">
                <div className="navbar-wrapper">
                    <p className="navbar-brand">Tableau de bord admin</p>
                </div>
                <div>
                    <ul className="navbar-nav">
                        <li className="nav-item dropdown">
                            <a className="nav-link" href="#" id="navbarDropdownProfile" data-toggle="dropdown"
                               aria-haspopup="true" aria-expanded="false">
                                <i className="material-icons">person</i>
                                <p className="d-lg-none d-md-block">
                                    Account
                                </p>
                            </a>
                            <div className="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                                <Link className="dropdown-item" to="/admin/profile">Profil</Link>
                                <div className="dropdown-divider" />
                                <Link className="dropdown-item" to="/close">Se déconnecter</Link>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </header>

    )
}
