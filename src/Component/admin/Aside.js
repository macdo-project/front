import {Link} from "react-router-dom";

export default function Aside(){
    return (
        <aside>
            <div className="sidebar-header">
                <Link to="/admin">Administration</Link>
            </div>
            <div className="sidebar-wrapper">
                <ul className="nav">
                    <li className="nav-item active">
                        <Link className="nav-link" to="/admin">
                            <i className="material-icons">dashboard</i>
                            <p>Tableau de board</p>
                        </Link>
                    </li>
                    <li className="nav-item">
                        <Link className="nav-link" to="/admin/commands">
                            <i className="material-icons">add_task</i>
                            <p>Commandes</p>
                        </Link>
                    </li>
                    <li className="nav-item">
                        <Link className="nav-link" to="/admin/ingredients">
                            <i className="material-icons">kitchen</i>
                            <p>Ingrédients</p>
                        </Link>
                    </li>
                    <li className="nav-item">
                        <Link className="nav-link" to="/admin/products">
                            <i className="material-icons">fastfood</i>
                            <p>Produits</p>
                        </Link>
                    </li>
                    <li className="nav-item">
                        <Link className="nav-link" to="/admin/menus">
                            <i className="material-icons">menu_book</i>
                            <p>Menu</p>
                        </Link>
                    </li>
                    <li className="nav-item">
                        <Link className="nav-link" to="/admin/terminals">
                            <i className="material-icons">cast</i>
                            <p>Bornes</p>
                        </Link>
                    </li>
                </ul>
            </div>
        </aside>
    )
}

