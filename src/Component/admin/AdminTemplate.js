import {useEffect, useState} from "react";
import {Redirect} from "react-router-dom";

import TokenManager from "../../Services/TokenManager";

import Aside from './Aside';
import Header from './Header';

import '../../assets/style/admin/admin.scss';

export default function AdminTemplate(props) {
    let [redirect, setRedirect] = useState(false)
    useEffect(() => {
        if(!TokenManager.isSet() || !TokenManager.isAdmin()){
            setRedirect(true)
        }
    }, []);

    if (redirect){
        return <Redirect to='/'/>

    }else{
        return (
            <div className="wrapper" id="admin">
                <Aside />
                <main>
                    <Header />
                    <div className="content">
                        {props.children}
                    </div>
                </main>
            </div>
        )
    }
}
