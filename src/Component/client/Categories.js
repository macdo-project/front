export default function Categories(props){

    const handleClick = (id) => {
        props.switchCategory(id)
    }

    return (
        <div className="categories">
            {props.categories.map((item) => {
                return (<div key={item.id} onClick={() => handleClick(item.id)}><p>{item.name}</p><img src={'/img/' + item.image} alt=""/></div>)
            })}
        </div>
    );
}
