import Item from "./Item";

export default function Products(props){
    return (
        <div className="products">
            {props.currentProducts.map((item) => {
                return (<Item key={item.id} name={item.name} picture={item.picture} price={item.price}/>)
            })}

        </div>
    );
}
