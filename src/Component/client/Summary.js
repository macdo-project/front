export default function Summary(props){
    return (
        <div className="summary">
            <div className="commandeSummary">
                <div>
                    <p>x1 Menu Classique Cheese</p>
                    <p>
                        <span className="material-icons">subdirectory_arrow_right</span>
                        CheeseBurger
                    </p>
                    <p>
                        <span className="material-icons">subdirectory_arrow_right</span>
                        Grandes frites
                    </p>
                    <p>
                        <span className="material-icons">subdirectory_arrow_right</span>
                        Coca Cola 33cl
                    </p>
                    <p>
                        <span className="material-icons">subdirectory_arrow_right</span>
                        Sunday
                    </p>
                </div>
                <div>
                    <p>x1 Menu Special Bacon</p>
                    <p>
                        <span className="material-icons">subdirectory_arrow_right</span>
                        Supreme Burger
                    </p>
                    <p>
                        <span className="material-icons">subdirectory_arrow_right</span>
                        Potatoes
                    </p>
                    <p>
                        <span className="material-icons">subdirectory_arrow_right</span>
                        Coca Cola 33cl
                    </p>
                    <p>
                        <span className="material-icons">subdirectory_arrow_right</span>
                        MilkShake
                    </p>
                </div>
            </div>
            <div className="total">
                <p className="price">Total: 36,83€</p>
                <button type="button" className="btn btn-success btn-lg my-2">Commander</button>
                <button type="button" className="btn btn-danger btn-lg">Annuler</button>
            </div>
        </div>
    )
}
