import './App.scss';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Redirect
} from "react-router-dom";

import AdminTemplate from './Component/admin/AdminTemplate';
import KitchenScreen from "./Screens/Kitchen";
import ClientScreen from "./Screens/Client";
import AuthScreen from "./Screens/Auth";

import AdminCommands from "./Screens/admin/Commands";
import AdminIngredient from "./Screens/admin/Ingredient";
import AdminProduct from "./Screens/admin/Product";
import AdminMenu from "./Screens/admin/Menu";
import AdminTerminal from "./Screens/admin/Terminal";
import TokenManager from "./Services/TokenManager";

/*import openSocket from 'socket.io-client';
const socket = openSocket('http://localhost:8080');*/

function App() {

    return (
      <Router>
            <Switch>
                <Route path="/" exact component={AuthScreen} />
                <Route path="/client" component={ClientScreen} />
                <Route path="/kitchen" component={KitchenScreen} />

                <Route path="/admin">
                    <AdminTemplate>
                        <Route path="/admin/commands">
                            <AdminCommands/>
                        </Route>
                        <Route path="/admin/ingredients">
                            <AdminIngredient/>
                        </Route>
                        <Route path="/admin/products">
                            <AdminProduct/>
                        </Route>
                        <Route path="/admin/menus">
                            <AdminMenu/>
                        </Route>
                        <Route path="/admin/terminals">
                            <AdminTerminal/>
                        </Route>
                    </AdminTemplate>
                </Route>

                <Route path="/close" component={() => {
                    TokenManager.clear();
                    return <Redirect to='/'/>
                }}/>
            </Switch>
      </Router>
    );
}

export default App;
