import { createStore, combineReducers } from 'redux'
import ApiManager from "../Services/ApiManager";

let localProducts = JSON.parse(localStorage.getItem('products'));
let localCategories = JSON.parse(localStorage.getItem('categories'));

const globalState = {
    categories: localCategories || ApiManager.getCategories(),
    products: localProducts || ApiManager.getProducts(),
}

function global (state = globalState, action) {
    let categories = state.categories
    let products = state.products

    switch(action.type) {
        case 'ADD_PRODUCT':
            products.push({title: action, category: action, image: action})
            localStorage.setItem('movies', JSON.stringify(products))
            return { ...state, products: products }
        case 'DELETE_PRODUCT':
            products.splice(action.index, 1)
            localStorage.setItem('movies', JSON.stringify(products))

            return { ...state, products: products }

        default:
            console.log('-----')
            return state

    }

}

const store = createStore(combineReducers({
    global: global
}), window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__())

export default store
