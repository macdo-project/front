import {useEffect, useState} from "react";
import { useForm } from "react-hook-form";

import ApiManager from "../../Services/ApiManager";

export default function AdminMenu(){
    let [menus, setMenus] = useState([])
    const { register, handleSubmit } = useForm();

    useEffect(() => {
        ApiManager.getCategories().then((data) => {
            setMenus(data);
        })
    }, [])

    return (
        <div className="container-fluid">
            <div className="card">
                <div className="card-header card-header-primary">
                    <h3 className="card-title">Menu</h3>
                </div>
                <div className="card-body">
                    <form onSubmit={handleSubmit((data) => {
                        
                    })}>
                        <input className="form-control" name="firstName" ref={register} />
                        <button type="submit" className="btn btn-primary">Save</button>
                    </form>
                    <hr/>
                    <table className="table mt-3">
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>Name</th>
                            <th className="text-center">Image</th>
                            <th>actions</th>
                        </tr>
                        </thead>
                        <tbody>
                            {menus.map((menu) => {
                                return (
                                    <tr key={menu.id}>
                                        <td>{menu.id}</td>
                                        <td>{menu.name}</td>
                                        <td className="text-center"><img src={'/img/' + menu.image} alt=""/></td>
                                        <td className="action">
                                            <button className="btn btn-info"><span className="material-icons">create</span><p>Edit</p></button>
                                            <button className="btn btn-danger"><span className="material-icons">delete</span><p>Delete</p></button>
                                        </td>
                                    </tr>
                                )
                            })}
                        </tbody>
                    </table>

                </div>
            </div>
        </div>)
}
