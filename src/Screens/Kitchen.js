import {useEffect} from "react";

import '../assets/style/kitchen.scss';
import TokenManager from "../Services/TokenManager";
import {Link} from "react-router-dom";

export default function Kitchen(props) {
    useEffect(() => {
        if(!TokenManager.isSet()){
            props.history.push('/')
        }else if(!TokenManager.isKitchen()){
            props.history.push('/client')
        }
    }, []);

    return (<div className="wrapper" id="kitchen">
        <div className="top">
            <h1>Commandes</h1>
            <Link to="/close">Déconnexion</Link>
        </div>
        <div className="content">

        </div>
    </div>)
}
