import {useEffect, useState} from "react"
import "../App.scss";
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import FormControl from 'react-bootstrap/FormControl';
import InputGroup from 'react-bootstrap/InputGroup';

import ApiManager from "../Services/ApiManager";
import TokenManager from "../Services/TokenManager";


export default function AuthScreen(props) {
    useEffect(() => {
        if(TokenManager.isSet()){
            TokenManager.clear()
        }
    }, []);

    let [borneId, setBorneId] = useState("");
    let [email,setEmail] = useState("");
    let [password,setPassword] = useState("")


    return (
        <div className="wrapper">
            <div className="forms">
                <div className="authForm">
                    <p>ID Borne</p>
                    <InputGroup>
                        <InputGroup.Prepend>
                            <InputGroup.Text id="basic-addon1">#</InputGroup.Text>
                        </InputGroup.Prepend>
                        <FormControl type="text" placeholder="aa1122" value={borneId} onChange={(e) => setBorneId(e.target.value)} />
                    </InputGroup>
                    <Button variant="primary" type="submit" onClick={() => {
                        ApiManager.auth(borneId).then((token) => {
                            TokenManager.set(token);
                            if (TokenManager.isKitchen()){
                                props.history.push('/kitchen')
                            }else{
                                props.history.push('/client')
                            }
                        }).catch((err) => {
                            alert(err)
                        })
                    }}>
                        Enregistrer
                    </Button>
                </div>
                <div className="adminForm">
                    <p>Connexion au panel admin</p>
                    <Form.Control type="email" value={email} onChange={(e) => setEmail(e.target.value)} placeholder="Identifiant" />
                    <Form.Control type="password" value={password} onChange={(e) => setPassword(e.target.value)} placeholder="Mot de passe" />
                    <Button variant="primary" type="submit" onClick={() => {
                            ApiManager.login(email, password).then((token) => {
                                TokenManager.set(token);
                                console.log(token)
                                props.history.push('/admin')
                            }).catch((err) => {
                                alert(err)
                            })
                        }}>
                        Connexion
                    </Button>
                </div>
            </div>
        </div>
    )
}
