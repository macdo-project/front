import {useEffect, useState} from "react";

import TokenManager from "../Services/TokenManager";

import '../assets/style/client.scss';

import Categories from "../Component/client/Categories";
import Products from "../Component/client/Products";
import Summary from "../Component/client/Summary";
import ApiManager from "../Services/ApiManager";


export default function ClientScreen(props) {

    let [categories, setCategories] = useState([])

    let [currentCategories, setCurrentCategories] = useState(0)
    let [currentProducts, setCurrentProducts] = useState([])

    useEffect(() => {
        if(!TokenManager.isSet()){
            props.history.push('/')
        }
        ApiManager.getCategories().then((data) => {
            setCategories(data);
        })
    }, []);

    useEffect(() => {
        ApiManager.getProductsFromCategory(currentCategories).then(data => {
            setCurrentProducts(data)
        })
        console.log(currentProducts)
    }, [currentCategories])

    return (
        <div className="wrapper" id="client">
            <div className="row">
                <Categories categories={categories} switchCategory={(id) => {setCurrentCategories(id)}}/>
                <div className="main">
                    <Products currentProducts={currentProducts}/>
                    <Summary/>
                </div>
            </div>
        </div>
    )
}
